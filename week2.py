# from Crypto.Cipher import AES
# from Crypto import Random
# import codecs
# import binascii

# def encrypt_cbc(key,pt):
# 	print("encrypt_cbc")

# def encrypt_ctr(key,pt):
# 	print("encrypt_ctr")

# def encrypt(key,pt,mode):
# 	if mode == "CBC":
# 		encrypt_cbc(key,pt)
# 	elif mode == "CTR":
# 		encrypt_ctr(key,pt)
# 	else:
# 		print("Mode not implemented yet :(")
# 	#could add more

# def decrypt_cbc(key,pt):
# 	print("decrypt_cbc")
# 	print(key)
# 	print(pt)

# def decrypt_ctr(key,pt):
# 	print("decrypt_ctr")
# 	print(key)
# 	print(pt)

# def decrypt(key,pt,mode):
# 	if mode == "CBC":
# 		decrypt_cbc(key,pt)
# 	elif mode == "CTR":
# 		decrypt_ctr(key,pt)
# 	else:
# 		print("Mode not implemented yet :(")
# 	#could add more

# def main():
# 	key = codecs.decode(bytes(input("Key, in hex: ")))
# 	e_or_d = input("Encrypt (e) or decrypt (d): ")
# 	if(e_or_d == "e"):
# 		pt = codecs.decode(bytes(input("Plain text, in hex: ")))
# 		mode = input("Mode: ")
# 		encrypt(key,pt,mode)
# 	if(e_or_d == "d"):
# 		ct = codecs.decode(bytes(input("Cipher text, in hex: ")))
# 		mode = input("Mode: ")
# 		decrypt(key,ct,mode)

# if __name__ == '__main__':
# 	main()
# key = b'Sixteen byte key'
# iv = Random.new().read(AES.block_size)
# cipher = AES.new(key, AES.MODE_CFB, iv)
# msg = iv + cipher.encrypt(b'Attack at dawn')

# print(cipher.decrypt(msg))
# print(msg)

# codecs.decode(b
#1)Basic CBC mode encryption needs padding.
#2)Our implemenation uses rand. IV



from Crypto.Cipher import AES
import struct

class MyCounter:

	def __init__(self, nonce):
		"""Initialize the counter object.

		@nonce      An 8 byte binary string.
		"""
		assert(len(nonce)==8)
		self.nonce = nonce
		self.cnt = 0

	def __call__(self):
		"""Return the next 16 byte counter, as binary string."""
		righthalf = struct.pack('>Q',self.cnt)
		self.cnt += 1
		return self.nonce + righthalf

cipher_ctr = AES.new(key, mode=AES.MODE_CTR, counter=MyCounter(nonce))
plaintext = cipher_ctr.decrypt(ciphertext)